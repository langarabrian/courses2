var express = require('express');
var router = express.Router();
var getCourses = require('../db');

/* GET home page. */
router.get('/', function(req, res, next) {
    getCourses({},{ subject: 1, course: 1, title: 1 },function(result) {
        res.render('index', { title: 'Full Stack Citation Courses', result });
    });
});

module.exports = router;

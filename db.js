var MongoClient = require('mongodb').MongoClient

var db = null;

function getCourses(findArg, projectArg, callback)
{
    if ( db ) {
        db.collection('courses').find(findArg).project(projectArg).toArray(function (err, result) {
            if (err) throw err;

            callback(result);
        });  
    } else {
        // MONGODBURI of the form "mongodb+srv://<USERNAME>:<PASSWORD>@<HOSTNAME>"
        const uri = `${process.env.MONGODBURI}/${process.env.MONGODBNAME}?retryWrites=true&w=majority`;
        const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
        client.connect(function (err) {
            if (err) throw err;

            db = client.db(process.env.MONGODBNAME);

            getCourses(findArg, projectArg, callback);
        })
    }
}

module.exports = getCourses;
